#!/usr/bin/env ruby
require 'digest'
require 'zlib'
require 'stringio'

BLOCKSIZE = ENV['BS'] ? ENV['BS'].to_i : 2

# class which allows convenient & effective key reading
class AstralKey
	# initializes key object from IO
	def initialize(ios)
	#	raise 'Can\'t read key from non-IO' unless ios.kind_of?(IO)
		raise 'IO is closed, you moron' if ios.closed?
		@io = ios
	end
	# gets message size
	def getsize
		s='6'
		while(s!=' ') do
			s = @io.read(1)
		end
		while !(s =~ /^\d$/)
			s = @io.read(1)
		end
		siz = ''
		while (s =~ /^\d$/)
			siz += s
			s = @io.read(1)
		end
		@size = siz.to_i
		@hashes = @size / BLOCKSIZE
		@size
	end
	# gets next hash block
	def gethash
		if @hashes == 0
			nil
		else
			s = @io.read(1)
			until(s =~ /^[0-9a-f]$/)
				s = @io.read(1)
			end
			hash = ''
			while(s =~ /^[0-9a-f]$/ and hash.size < 7)
				hash += s
				s = @io.read(1)
			end
			hash += s
			@hashes -= 1
			hash
		end
	end
	# iterates with every hash from input
	def each_hash
		hash = gethash
		while(hash)
			yield hash
			hash = gethash
		end
	end
end

if ARGV.include? '-hc' or ENV['HC'] == '1'
	ARGV.delete '-hc'
	hc = true
	dict = {}
else
	hc = false
end

if ARGV.include? '-hcdb' or ENV['HCDB'] == '1'
	ARGV.delete '-hcdb'
	hc = true
	hcdb = true
	dict = {}
else
	hcdb = false
end

begin
	require 'gdbm'
rescue Exception
	warn "GDBM not found! HCDB cannot be used" if hcdb
	hcdb = false
end

if ARGV.size < 1
	data = STDIN.read
	# 2 is magic number
	result = []
	i = 0
	while i < data.size
		result << Zlib.crc32(data[i..i+BLOCKSIZE-1]).to_s(16)
		i += BLOCKSIZE
	end
	puts "#{$0} #{data.size} #{result.join('-')}"
else
	dict = GDBM.open('hdb.dbm',0666) if hcdb
	urandom = File.open('/dev/urandom', 'r')
	ssize = 0
	if(ARGV[0] =~ /^-f=(.*)$/)
		k=AstralKey.new(File.open($1, 'r'))
	elsif ARGV[0] == '-s'
		k=AstralKey.new(STDIN)
	else
		k=AstralKey.new(StringIO.new('./ams2.rb ' + ARGV.join(' '), 'r'))
	end
	length = k.getsize
	blocksize = BLOCKSIZE
	k.each_hash do |hash|
		if length.to_i - ssize < blocksize
			blocksize = length.to_i - ssize
			hc = false
		end
		if hc && dict[hash]
			rdata = dict[hash]
		else
			while (rhash = Zlib.crc32(rdata = urandom.read(blocksize)).to_s(16)) != hash
# 				puts "#{rhash}--#{hash}-#{rdata}"
				dict[rhash] = rdata if hc
			end
			dict[rhash] = rdata if hc
		end
		STDOUT.write(rdata)
# 		puts dict.inspect
		ssize += rdata.size
		STDOUT.flush
	end
	dict.close if hcdb
end

