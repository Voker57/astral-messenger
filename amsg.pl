#!/usr/bin/env perl

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

use Digest::CRC qw(crc32_hex);
use Digest::SHA qw(sha512_hex);

$version='1.1';
sub ahash($)
{
	# Inserts argument into AstralNet and returns handle
	$hash=crc32_hex($_[0]).' '.sha512_hex($_[0]).' '.length($_[0]);
	return $hash;
}

if(!$ARGV[0])
{
	while(<>) { $file=$file.=$_; };
	chomp($file);
	print "./amsg.pl ".ahash($file)."\n";
} elsif(($ARGV[0] =~ /^\w+$/) && ($ARGV[1] =~ /^\w+$/) && ($ARGV[2] =~ /^\d+$/))
{
	open(URANDOM, '</dev/urandom') or die('Failed to connect to AstralNet!');
	print "Searching for $ARGV[0] sha512=$ARGV[1] sized $ARGV[2]...\n";
	$stime=time();
	$result=0;
	while(!$result)
	{
		read(URANDOM, $hh, $ARGV[2]);
		$crcash=crc32_hex($hh);
		print "\n$hh..." if $ARGV[3];
		if($crcash eq $ARGV[0])
		{
			print "Suspected..." if $ARGV[3];
			$shash=sha512_hex($hh);
			if($shash eq $ARGV[1])
			{
				print "$shash-Here's it!" if $ARGV[3];
				$result=$hh;
			} else {print "$shash-Failed."  if $ARGV[3]; }
		} else {print "$crcash-Not even close!" if $ARGV[3]; }
	}
	print "File found in ".(time-$stime)."s: $result\n";
} else
{
	print("AstralMessenger v$version
	Usage:
	Pipe a file to insert it into AstralNet
	Examples:
		echo 'hi' | ./amsg.pl
		cat pagefile.sys | ./amsg.pl
	or
	./amsg CRC32 SHA512 SIZE [VERBOSE]
	to retrieve a file
	Examples:
		./amsg.pl af941a3a 7de896b588a8efaf14ecf59bcf17e883194ecbc7115e259b435551d69dbaf17741f13aaab0a759567d9b6ff361b5354edb35204d41c651bb944d2d5405e5b1de 2
	");
}